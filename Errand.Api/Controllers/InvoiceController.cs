﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Errand.Application.Invoice;
using Errand.Application.Invoice.Commands;
using Errand.Application.Invoice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Errand.Api.Controllers
{
    public class InvoiceController : BaseController
    {
        [Authorize]
        [HttpPost("/api/invoice/create")]
        public async Task<ActionResult<InvoiceDto>> Create([FromBody] InvoiceDto invoiceDto)
        {
            return Ok(await Mediator.Send(new GenerateInvoiceCommand(invoiceDto)));
        }

        [Authorize]
        [HttpGet("/api/invoices")]
        public async Task<List<InvoiceDto>> GetInvoices()
        {
            return await Mediator.Send(new GetInvoicesQuery());
        }

        [Authorize]
        [HttpGet("/api/invoice/{id}")]
        public async Task<InvoiceDetailsDto> GetInvoice(int id)
        {
            return await Mediator.Send(new GetInvoiceQuery(id));
        }
    }
}
