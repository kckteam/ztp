﻿using System.Threading.Tasks;
using Errand.Application.Product.Commands;
using Errand.Application.Product;
using Errand.Application.Product.Queries.GetAllProducts;
using Errand.Application.Product.Queries.GetProduct;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Errand.Api.Controllers
{
    public class ProductController : BaseController
    {
        [Authorize]
        [HttpGet("/api/products")]
        public async Task<ActionResult<ProductListViewModel>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllProductsQuery()));
        }
        
        [Authorize]
        [HttpGet("/api/product/{id}")]
        public async Task<ActionResult<ProductDto>> Get(int id)
        {
            return Ok(await Mediator.Send(new GetProductQuery(id)));
        }

        [Authorize]
        [HttpPost("/api/product/create")]
        public async Task<ActionResult<ProductDto>> Create([FromBody] ProductDto productDto)
        {
            return Ok(await Mediator.Send(new CreateProductCommand(productDto)));
        }
    }
}
