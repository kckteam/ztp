﻿using System.Threading.Tasks;
using Errand.Application.Order;
using Errand.Application.Order.Commands.CreateOrder;
using Errand.Application.Order.Commands.ProcessOrder.Command;
using Errand.Application.Order.Queries.GetAllOrders;
using Errand.Application.Order.Queries.GetOrder;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Errand.Api.Controllers
{
    public class OrderController : BaseController
    {
        [Authorize]
        [HttpPost("/api/order/process")]
        public async Task<ActionResult<OrderDto>> Process([FromBody] OrderDto order)
        {
            return Ok(await Mediator.Send(new ProcessOrderCommand(order)));
        }

        [Authorize]
        [HttpGet("/api/orders")]
        public async Task<ActionResult<OrderListViewModel>> Orders()
        {
            return Ok(await Mediator.Send(new GetAllOrdersQuery()));
        }

        [Authorize]
        [HttpGet("/api/order/{id}")]
        public async Task<ActionResult<OrderListViewModel>> Order(int id)
        {
            return Ok(await Mediator.Send(new GetOrderQuery(id)));
        }
    }
}