﻿using System;
using System.Threading.Tasks;
using Errand.Application.Account;
using Errand.Application.Account.Command;
using Errand.Application.Account.Query;
using Microsoft.AspNetCore.Mvc;

namespace Errand.Api.Controllers
{
    public class AccountController : BaseController
    {
        [HttpPost("/api/register")]
        public async Task<IActionResult> Register([FromBody]RegisterCommand customer)
        {
            return Ok(await Mediator.Send(customer));
        }

        [HttpPost("/api/login")]
        public async Task<IActionResult> Login([FromBody]GetTokenQuery model)
        {
            try
            {
                TokenViewModel token = await Mediator.Send(model);
                return new ObjectResult(token);
            }
            catch (UnauthorizedAccessException ex)
            {
                return Unauthorized();
            }
        }
    }
}
