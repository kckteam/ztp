﻿using System;

namespace Errand.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
