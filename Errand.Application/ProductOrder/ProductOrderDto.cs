﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Errand.Application.ProductOrder
{
    public class ProductOrderDto
    {
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }

        public static Expression<Func<Errand.Domain.Entities.ProductOrder, ProductOrderDto>> Projection
        {
            get
            {
                return p => new ProductOrderDto
                {
                    OrderId = p.OrderId,
                    ProductId = p.ProductId,
                    Quantity = p.Quantity
                };
            }
        }
    }
}
