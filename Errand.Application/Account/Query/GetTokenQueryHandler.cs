﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Account.Helper;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Errand.Application.Account.Query
{
    public class GetTokenQueryHandler : IRequestHandler<GetTokenQuery, TokenViewModel>
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger _logger;
        private readonly JwtSettings _jwt;

        public GetTokenQueryHandler(IUnitOfWork uow, ILogger logger, IOptions<JwtSettings> jwt)
        {
            _uow = uow;
            _logger = logger;
            _jwt = jwt.Value;
        }
        public async Task<TokenViewModel> HandleAsync(GetTokenQuery request, CancellationToken cancellationToken)
        {
            var user = await _uow.UserRepository.Query().FirstOrDefaultAsync(x => x.Username.Equals(request.Login));
            if (user == null || !PasswordHelper.ValidatePassword(request.Password, user.Password))
            {
                throw new UnauthorizedAccessException();
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwt.Key);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddMinutes(20),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
            };
            var result = tokenHandler.CreateToken(tokenDescriptor);

            return new TokenViewModel { Token = "Bearer " + tokenHandler.WriteToken(result) };
        }
    }
}
