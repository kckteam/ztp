﻿using System;
using System.Collections.Generic;
using System.Text;
using Errand.Application.Interfaces;

namespace Errand.Application.Account.Query
{
    public class GetTokenQuery : IRequest<TokenViewModel>
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
