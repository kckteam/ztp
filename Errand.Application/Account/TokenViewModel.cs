﻿namespace Errand.Application.Account
{
    public class TokenViewModel
    {
        public string Token { get; set; }
    }
}
