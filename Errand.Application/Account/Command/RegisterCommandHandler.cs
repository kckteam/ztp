﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Account.Helper;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Errand.Domain.Unit;

namespace Errand.Application.Account.Command
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand>
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger _logger;

        public RegisterCommandHandler(IUnitOfWork uow, ILogger logger)
        {
            _uow = uow;
            _logger = logger;
        }
        public async Task<VoidResult> HandleAsync(RegisterCommand request, CancellationToken cancellationToken)
        {
            var hash = new HashedPassword(PasswordHelper.CreateHash(request.Password));
            request.Password = hash.ToSaltedPassword();
            _uow.UserRepository.Insert(new Domain.Entities.User
            {
                Created = DateTime.Now,
                Username = request.Login,
                Password = request.Password
            });
            await _uow.SaveChangesAsync();
            _logger.Log("Account", "INFO", "Account :" + request.Login + " created.");

            return await VoidResult.Task;
        }
    }
}
