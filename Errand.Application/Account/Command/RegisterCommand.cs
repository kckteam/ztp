﻿using Errand.Application.Interfaces;

namespace Errand.Application.Account.Command
{
    public class RegisterCommand : IRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
