﻿using System.Threading.Tasks;
using Errand.Application.Order;

namespace Errand.Application.Interfaces
{
    public interface IState
    {
        Task DoAction(OrderDto request);
    }
}
