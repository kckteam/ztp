﻿namespace Errand.Application.Interfaces
{
    using System;
    using System.Linq;
    using Errand.Domain.Entities.Base;

    public interface IRepository<TEntity, TKey> : IDisposable
        where TEntity : BaseEntity<TKey>
        where TKey : IComparable
    {
        void Delete(TEntity entity);

        TEntity Insert(TEntity entity);

        IQueryable<TEntity> Query();

        TEntity Update(TEntity entity);
    }
}
