﻿using Errand.Domain.Unit;

namespace Errand.Application.Interfaces
{
    public interface IRequest<out TResponse> : IBaseRequest { }

    public interface IBaseRequest { }

    public interface IRequest : IRequest<VoidResult> { }
}
