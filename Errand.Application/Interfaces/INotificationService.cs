﻿namespace Errand.Application.Interfaces
{
    using System.Threading.Tasks;
    using Errand.Application.Notifications.Models;

    public interface INotificationService
    {
        Task SendAsync(Message message);
    }
}
