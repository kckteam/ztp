﻿namespace Errand.Application.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    public interface IMediator
    {
        Task<TResponse> Send<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = default);
    }
}
