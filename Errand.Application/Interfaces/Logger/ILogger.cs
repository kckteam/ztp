﻿namespace Errand.Application.Interfaces.Logger
{
    public interface ILogger
    {
        bool Log(string app, string key, string cause);
    }
}
