﻿namespace Errand.Application.Interfaces.Logger
{
    using System.Threading.Tasks;

    public interface IContentWriter
    {
        Task<bool> Write(string content);
    }
}
