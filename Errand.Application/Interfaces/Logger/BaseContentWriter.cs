﻿namespace Errand.Application.Interfaces.Logger
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;

    public abstract class BaseContentWriter : IContentWriter
    {
        private ConcurrentQueue<string> queue = new ConcurrentQueue<string>();
        private Object _lock = new Object();

        public BaseContentWriter() { }
        //---- Write to Media 
        public abstract bool WriteToMedia(string logcontent);

        async Task Flush()
        {
            string content;
            int count = 0;
            while (queue.TryDequeue(out content) && count <= 10)
            {
                WriteToMedia(content);
                count++;
            }
        }

        public async Task<bool> Write(string content)
        {
            queue.Enqueue(content);
            if (queue.Count <= 3)
                return true;
            lock (_lock)
            {
                Task temp = Task.Run(() => Flush());
                Task.WaitAll(new Task[] { temp });
            }
            return true;
        }

    }
}
