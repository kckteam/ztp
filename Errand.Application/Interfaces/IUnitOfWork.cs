﻿namespace Errand.Application.Interfaces
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Errand.Domain.Entities;
    using Errand.Domain.Entities.Base;

    public interface IUnitOfWork : IDisposable
    {
        IRepository<User, int> UserRepository { get;}
        IRepository<ProductOrder, int> ProductOrderRepository { get;}
        IRepository<Product, int> ProductRepository { get;}
        IRepository<Invoice, int> InvoiceRepository { get;}
        IRepository<Order, int> OrderRepository { get;}

        void Dispose(bool disposing);

        IRepository<TEntity, TKey> Repository<TEntity, TKey>() where TEntity : BaseEntity<TKey> where TKey : IComparable;

        int SaveChanges();

        Task<int> SaveChangesAsync();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
