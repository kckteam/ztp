﻿using System.Threading;
using System.Threading.Tasks;
using Errand.Domain.Unit;

namespace Errand.Application.Interfaces
{
    public interface IRequestHandler<in TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        Task<TResponse> HandleAsync(TRequest request, CancellationToken cancellationToken);
    }

    public interface IRequestHandler<in TRequest> : IRequestHandler<TRequest, VoidResult>
        where TRequest : IRequest<VoidResult>
    {
    }

    //public abstract class AsyncRequestHandler<TRequest> : IRequestHandler<TRequest>
    //    where TRequest : IRequest
    //{
    //    async Task<VoidResult> IRequestHandler<TRequest, VoidResult>.Handle(TRequest request, CancellationToken cancellationToken)
    //    {
    //        await Handle(request, cancellationToken).ConfigureAwait(false);
    //        return VoidResult.Value;
    //    }

    //    protected abstract Task Handle(TRequest request, CancellationToken cancellationToken);
    //}

    public abstract class RequestHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        Task<TResponse> IRequestHandler<TRequest, TResponse>.HandleAsync(TRequest request, CancellationToken cancellationToken)
            => Task.FromResult(Handle(request));

        protected abstract TResponse Handle(TRequest request);
    }

    public abstract class RequestHandler<TRequest> : IRequestHandler<TRequest>
        where TRequest : IRequest
    {
        Task<VoidResult> IRequestHandler<TRequest, VoidResult>.HandleAsync(TRequest request, CancellationToken cancellationToken)
        {
            Handle(request);
            return VoidResult.Task;
        }

        protected abstract void Handle(TRequest request);
    }
}
