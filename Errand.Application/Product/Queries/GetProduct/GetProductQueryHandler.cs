﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Interfaces;

namespace Errand.Application.Product.Queries.GetProduct
{
    public class GetProductQueryHandler : IRequestHandler<GetProductQuery, ProductDto>
    {
        private readonly IUnitOfWork _uow;

        public GetProductQueryHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Task<ProductDto> HandleAsync(GetProductQuery request, CancellationToken cancellationToken)
        {
            var product = _uow.ProductRepository.Query().Where(x => x.Id == request.Id).Select(ProductDto.Projection).FirstOrDefault();

            return Task.FromResult<ProductDto>(product);
        }
    }
}
