﻿using Errand.Application.Interfaces;

namespace Errand.Application.Product.Queries.GetProduct
{
    public class GetProductQuery : IRequest<ProductDto>
    {
        public int Id { get; set; }

        public GetProductQuery(int id)
        {
            Id = id;
        }
    }
}
