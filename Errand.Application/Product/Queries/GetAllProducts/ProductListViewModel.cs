﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Errand.Application.Product.Queries.GetAllProducts
{
    public class ProductListViewModel
    {
        public IEnumerable<ProductDto> Products { get; set; }
    }
}
