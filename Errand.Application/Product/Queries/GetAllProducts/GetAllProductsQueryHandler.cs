﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Microsoft.EntityFrameworkCore;

namespace Errand.Application.Product.Queries.GetAllProducts
{
    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, ProductListViewModel>
    {
        private readonly IUnitOfWork _uow;
        
        private readonly ILogger _logger;
        public GetAllProductsQueryHandler(ILogger logger, IUnitOfWork uow)
        {
            _logger = logger;
            _uow = uow;
        }

        public Task<ProductListViewModel> HandleAsync(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            var products = _uow.ProductRepository.Query().Select(ProductDto.Projection).ToList();

            _logger.Log("Errand", "INFO", "Invoke GetAllProductsQueryHandler");

            return Task.FromResult<ProductListViewModel>(new ProductListViewModel
            {
                Products = products
            });
        }
    }
}
