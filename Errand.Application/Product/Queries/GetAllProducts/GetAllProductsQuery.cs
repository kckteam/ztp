﻿using Errand.Application.Interfaces;

namespace Errand.Application.Product.Queries.GetAllProducts
{
    public class GetAllProductsQuery : IRequest<ProductListViewModel>
    {
    }
}
