﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Errand.Application.Product
{
    public class ProductDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
        public int Quantity { get; set; }

        public static Expression<Func<Errand.Domain.Entities.Product, ProductDto>> Projection
        {
            get
            {
                return p => new ProductDto
                {
                    Name = p.Name,
                    Price = p.Price,
                    ProductId = p.Id,
                    Quantity = p.Quantity,
                    VAT = p.VAT
                };
            }
        }

        public static ProductDto Create(Errand.Domain.Entities.Product product)
        {
            return Projection.Compile().Invoke(product);
        }
    }
}
