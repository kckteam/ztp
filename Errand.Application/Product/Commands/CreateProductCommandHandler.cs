﻿using Errand.Application.Interfaces;
using Errand.Domain.Unit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Errand.Application.Product.Commands
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand>
    {
        private readonly IUnitOfWork _uow;

        public CreateProductCommandHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Task<VoidResult> HandleAsync(CreateProductCommand request, CancellationToken cancellationToken)
        {
            Errand.Domain.Entities.Product product = new Errand.Domain.Entities.Product
            {
                Created = DateTime.Now,
                Name = request.ProductDto.Name,
                Price = request.ProductDto.Price,
                Quantity = request.ProductDto.Quantity,
                VAT = request.ProductDto.VAT
            };

            _uow.ProductRepository.Insert(product);

            _uow.SaveChanges();

            return VoidResult.Task;
        }
    }
}
