﻿using Errand.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Errand.Application.Product.Commands
{
    public class CreateProductCommand : IRequest
    {
        public ProductDto ProductDto { get; set; }

        public CreateProductCommand(ProductDto productDto)
        {
            ProductDto = productDto;
        }
    }
}
