﻿using System.Collections.Generic;
using Errand.Application.Product;

namespace Errand.Application.Order
{
    public class OrderDetailsDto
    {
        public string ClientName { get; set; }
        public string OrderState { get; set; }
        public IEnumerable<ProductDto> Products { get; set; }
        public decimal TotalPrice { get; set; }

    }
}
