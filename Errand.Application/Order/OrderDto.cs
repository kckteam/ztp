﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Errand.Application.Order
{
    public class OrderDto
    {
        public string ClientName { get; set; }
        public string OrderState { get; set; }
        public int OrderId { get; set; }
        public Application.ProductOrder.ProductOrderDto[] ProductOrders { get; set; }

        public static Expression<Func<Errand.Domain.Entities.Order, OrderDto>> Projection
        {
            get
            {
                return p => new OrderDto
                {
                    ClientName = p.ClientName,
                    OrderState = p.OrderState,
                    OrderId = p.Id
                };
            }
        }

        public static OrderDto Create(Errand.Domain.Entities.Order order)
        {
            return Projection.Compile().Invoke(order);
        }
    }
}
