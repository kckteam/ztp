﻿namespace Errand.Application.Order.Queries.GetOrder
{
    using Errand.Application.Interfaces;

    public class GetOrderQuery : IRequest<OrderDetailsDto>
    {
        public int OrderId { get; set; }

        public GetOrderQuery(int id)
        {
            OrderId = id;
        }
    }
}
