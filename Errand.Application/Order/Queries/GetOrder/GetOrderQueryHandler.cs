﻿namespace Errand.Application.Order.Queries.GetOrder
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Errand.Application.Interfaces;
    using Errand.Application.Interfaces.Logger;
    using Errand.Application.Product;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;

    public class GetOrderQueryHandler : IRequestHandler<GetOrderQuery, OrderDetailsDto>
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _uow;
        public GetOrderQueryHandler(ILogger logger, IUnitOfWork uow)
        {
            _uow = uow;
            _logger = logger;
        }
        public async Task<OrderDetailsDto> HandleAsync(GetOrderQuery request, CancellationToken cancellationToken)
        {
            var order = await _uow.OrderRepository.Query().FirstOrDefaultAsync(x => x.Id.Equals(request.OrderId));
            var orderProducts = await _uow.ProductOrderRepository.Query().Where(x => x.OrderId.Equals(order.Id)).ToListAsync();
            var products = await _uow.ProductRepository.Query()
                .Where(x => orderProducts.Any(z => z.ProductId.Equals(x.Id)))
                .Select(l => new ProductDto
                {
                    Name = l.Name,
                    Price = l.Price,
                    Quantity = orderProducts.Where(op => op.ProductId.Equals(l.Id)).Select(op => op.Quantity).FirstOrDefault(),
                    VAT = l.VAT
                }).ToListAsync();
            return await Task.FromResult<OrderDetailsDto>(new OrderDetailsDto
            {
                ClientName = order.ClientName,
                OrderState = order.OrderState,
                Products = products,
                TotalPrice = products.Sum(x => (x.Price * x.Quantity)*(x.VAT/100) + (x.Price * x.Quantity))
            });
        }
    }
}
