﻿namespace Errand.Application.Order.Queries.GetAllOrders
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Errand.Application.Interfaces;
    using Errand.Application.Interfaces.Logger;
    using Microsoft.EntityFrameworkCore;

    public class GetAllOrdersQueryHandler : IRequestHandler<GetAllOrdersQuery, OrderListViewModel>
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _uow;

        public GetAllOrdersQueryHandler(ILogger logger, IUnitOfWork uow)
        {
            _logger = logger;
            _uow = uow;
        }
        public async Task<OrderListViewModel> HandleAsync(GetAllOrdersQuery request, CancellationToken cancellationToken)
        {
            var orders = await _uow.OrderRepository.Query().Select(OrderDto.Projection).ToListAsync();
            return await Task.FromResult<OrderListViewModel>(new OrderListViewModel
            {
                Orders = orders 
            });
        }
    }
}
