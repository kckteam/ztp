﻿namespace Errand.Application.Order.Queries.GetAllOrders
{
    using System.Collections.Generic;

    public class OrderListViewModel
    {
        public IEnumerable<OrderDto> Orders { get; set; }
    }
}
