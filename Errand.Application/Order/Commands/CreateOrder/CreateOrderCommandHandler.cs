﻿using Errand.Application.Interfaces;
using Errand.Domain.Unit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Errand.Application.Order.Commands.CreateOrder
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand>
    {
        private readonly IUnitOfWork _uow;

        public CreateOrderCommandHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Task<VoidResult> HandleAsync(CreateOrderCommand request, CancellationToken cancellationToken)
        {
           

            return VoidResult.Task;
        }
    }
}
