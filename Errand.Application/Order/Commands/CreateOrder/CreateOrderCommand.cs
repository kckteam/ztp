﻿using Errand.Application.Interfaces;

namespace Errand.Application.Order.Commands.CreateOrder
{
    public class CreateOrderCommand : IRequest
    {
        public OrderDto Order { get; set; }

        public CreateOrderCommand(OrderDto order)
        {
            Order = order;
        }
    }
}
