﻿using Errand.Application.Interfaces;

namespace Errand.Application.Order.Commands.ProcessOrder.Command
{
    public class ProcessOrderCommand : IRequest
    {
        public OrderDto Order { get; set; }

        public ProcessOrderCommand(OrderDto order)
        {
            Order = order;
        }
    }
}
