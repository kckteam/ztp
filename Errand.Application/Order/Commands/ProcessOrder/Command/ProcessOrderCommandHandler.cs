﻿using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Errand.Application.Order.Commands.State;
using Errand.Domain.Unit;

namespace Errand.Application.Order.Commands.ProcessOrder.Command
{
    public class ProcessOrderCommandHandler : IRequestHandler<ProcessOrderCommand>
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger _logger;
        public ProcessOrderCommandHandler(IUnitOfWork uow, ILogger logger)
        {
            _logger = logger;
            _uow = uow;
        }
        public async Task<VoidResult> HandleAsync(ProcessOrderCommand request, CancellationToken cancellationToken)
        {
            var context = new StateContext(request.Order, _uow, _logger);
            await context.Request();

            return await VoidResult.Task;
        }
    }
}
