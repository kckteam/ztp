﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Errand.Application.Product;
using Errand.Domain.Unit;
using Microsoft.EntityFrameworkCore;

namespace Errand.Application.Order.Commands.FinalizeOrder
{
    public class FinalizeOrderCommandHandler : IRequestHandler<FinalizeOrderCommand>
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _uow;
        public FinalizeOrderCommandHandler(ILogger logger, IUnitOfWork uow)
        {
            _logger = logger;
            _uow = uow;
        }
        public async Task<VoidResult> HandleAsync(FinalizeOrderCommand request, CancellationToken cancellationToken)
        {
            var order = await _uow.OrderRepository.Query().FirstOrDefaultAsync(x => x.Id.Equals(request.OrderId));
            var orderProducts = await _uow.ProductOrderRepository.Query().Where(x => x.OrderId.Equals(order.Id)).ToListAsync();
            var products = await _uow.ProductRepository.Query()
                .Where(x => orderProducts.Any(z => z.ProductId.Equals(x.Id))).ToListAsync();
            orderProducts.ForEach(z => products.FirstOrDefault(x => x.Id.Equals(z.ProductId)).Quantity -= z.Quantity);
            products.ForEach(x => _uow.ProductRepository.Update(x));
            order.OrderState = "Finalized";
            await _uow.SaveChangesAsync();
            await _uow.SaveChangesAsync();
            _logger.Log("OrderFinalize", "INFO", "Order " + order.Id + " finalized.");

            return await VoidResult.Task;
        }
    }
}
