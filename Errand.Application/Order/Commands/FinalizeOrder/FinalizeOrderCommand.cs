﻿using System;
using System.Collections.Generic;
using System.Text;
using Errand.Application.Interfaces;

namespace Errand.Application.Order.Commands.FinalizeOrder
{
    public class FinalizeOrderCommand : IRequest
    {
        public int OrderId { get; set; }

        public FinalizeOrderCommand(int id)
        {
            OrderId = id;
        }
    }
}
