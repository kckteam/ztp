﻿using System;
using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Errand.Application.Order.Commands.CreateOrder;

namespace Errand.Application.Order.Commands.State
{
    public class CreateOrderState : IState
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger _logger;
        public CreateOrderState(IUnitOfWork uow, ILogger logger)
        {
            _uow = uow;
            _logger = logger;
        }
        public async Task DoAction(OrderDto request)
        {
            Errand.Domain.Entities.Order order = new Errand.Domain.Entities.Order
            {
                Created = DateTime.Now,
                ClientName = request.ClientName,
                OrderState = "Created"
            };

            _uow.OrderRepository.Insert(order);

            foreach (var item in request.ProductOrders)
            {
                _uow.ProductOrderRepository.Insert(new Domain.Entities.ProductOrder
                {
                    Created = DateTime.Now,
                    OrderId = order.Id,
                    ProductId = item.ProductId,
                    Quantity = item.Quantity
                });
            }

            await _uow.SaveChangesAsync();
        }
    }
}
