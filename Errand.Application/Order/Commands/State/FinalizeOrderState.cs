﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Errand.Application.Order.Commands.CreateOrder;
using Microsoft.EntityFrameworkCore;

namespace Errand.Application.Order.Commands.State
{
    public class FinalizeOrderState : IState
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger _logger;
        public FinalizeOrderState(IUnitOfWork uow, ILogger logger)
        {
            _uow = uow;
            _logger = logger;
        }
        public async Task DoAction(OrderDto request)
        {
            var order = await _uow.OrderRepository.Query().FirstOrDefaultAsync(x => x.Id.Equals(request.OrderId));
            var orderProducts = await _uow.ProductOrderRepository.Query().Where(x => x.OrderId.Equals(order.Id)).ToListAsync();
            var products = await _uow.ProductRepository.Query()
                .Where(x => orderProducts.Any(z => z.ProductId.Equals(x.Id))).ToListAsync();
            orderProducts.ForEach(z => products.FirstOrDefault(x => x.Id.Equals(z.ProductId)).Quantity -= z.Quantity);
            products.ForEach(x => _uow.ProductRepository.Update(x));
            order.OrderState = "Finalized";

            await _uow.SaveChangesAsync();
            _logger.Log("OrderFinalize", "INFO", "Order " + order.Id + " finalized.");
        }
    }
}
