﻿using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Errand.Application.Order.Commands.CreateOrder;
using Microsoft.EntityFrameworkCore;

namespace Errand.Application.Order.Commands.State
{
    public class StateContext
    {
        private IState _state;
        private readonly IUnitOfWork _uow;
        private readonly ILogger _logger;
        private OrderDto _order;

        public StateContext(OrderDto order, IUnitOfWork uow, ILogger logger)
        {
            _uow = uow;
            _logger = logger;
            _order = order;
            var result = _uow.OrderRepository.Query().FirstOrDefaultAsync(x => x.Id.Equals(order.OrderId)).GetAwaiter().GetResult();
            if (_order.OrderId == 0 || result == null || result.OrderState == "Finalized")
            {
                _state = new CreateOrderState(_uow, _logger);
            }
            else
            {
                _state = new FinalizeOrderState(_uow, _logger);
            }
        }

        public async Task Request()
        {
            await _state.DoAction(_order);
        }
    }
}
