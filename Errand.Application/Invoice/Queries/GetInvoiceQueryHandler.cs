﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Interfaces.Logger;
using Errand.Application.Order.Queries.GetOrder;
using Microsoft.EntityFrameworkCore;

namespace Errand.Application.Invoice.Queries
{
    public class GetInvoiceQueryHandler : IRequestHandler<GetInvoiceQuery, InvoiceDetailsDto>
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        public GetInvoiceQueryHandler(IUnitOfWork uow, ILogger logger, IMediator mediator)
        {
            _uow = uow;
            _logger = logger;
            _mediator = mediator;
        }
        public async Task<InvoiceDetailsDto> HandleAsync(GetInvoiceQuery request, CancellationToken cancellationToken)
        {
            var invoice = await  _uow.InvoiceRepository.Query().FirstOrDefaultAsync(x => x.Id.Equals(request.InvoiceId));
            if (invoice != null)
            {
                var orderDetails = await _mediator.Send(new GetOrderQuery(invoice.OrderId));

                return new InvoiceDetailsDto
                {
                    Order = orderDetails,
                    OrderId = invoice.OrderId,
                    InvoiceId = invoice.Id
                };
            }
            return new InvoiceDetailsDto();
        }
    }
}
