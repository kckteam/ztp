﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Errand.Application.Invoice.Queries
{
    public class GetInvoicesQueryHandler : IRequestHandler<GetInvoicesQuery, List<InvoiceDto>>
    {
        private readonly IUnitOfWork _uow;

        public GetInvoicesQueryHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public async Task<List<InvoiceDto>> HandleAsync(GetInvoicesQuery request, CancellationToken cancellationToken)
        {
            return await _uow.InvoiceRepository.Query().Select(x => new InvoiceDto
            {
                OrderId = x.OrderId,
                FullPrice = x.FullPrice,
                InvoiceId = x.Id
            }).ToListAsync();
        }
    }
}
