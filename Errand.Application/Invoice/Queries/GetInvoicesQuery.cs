﻿using System;
using System.Collections.Generic;
using System.Text;
using Errand.Application.Interfaces;

namespace Errand.Application.Invoice.Queries
{
    public class GetInvoicesQuery : IRequest<List<InvoiceDto>>
    {
    }
}
