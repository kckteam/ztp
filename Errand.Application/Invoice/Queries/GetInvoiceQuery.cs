﻿using Errand.Application.Interfaces;

namespace Errand.Application.Invoice.Queries
{
    public class GetInvoiceQuery : IRequest<InvoiceDetailsDto>
    {
        public int InvoiceId { get; set; }

        public GetInvoiceQuery(int id)
        {
            InvoiceId = id;
        }
    }
}
