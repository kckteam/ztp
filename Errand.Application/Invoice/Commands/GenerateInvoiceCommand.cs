﻿using Errand.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Errand.Application.Invoice.Commands
{
    public class GenerateInvoiceCommand : IRequest
    {
        public InvoiceDto InvoiceDto { get; set; }

        public GenerateInvoiceCommand(InvoiceDto invoiceDto)
        {
            InvoiceDto = invoiceDto;
        }
    }
}
