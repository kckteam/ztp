﻿using Errand.Application.Interfaces;
using Errand.Domain.Unit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using Errand.Application.ProductOrder;
using Microsoft.EntityFrameworkCore.Internal;
using Errand.Application.Product;

namespace Errand.Application.Invoice.Commands
{
    public class GenerateInvoiceCommandHandler : IRequestHandler<GenerateInvoiceCommand>
    {
        private readonly IUnitOfWork _uow;

        public GenerateInvoiceCommandHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Task<VoidResult> HandleAsync(GenerateInvoiceCommand request, CancellationToken cancellationToken)
        {
            Errand.Domain.Entities.Invoice invoice = new Errand.Domain.Entities.Invoice
            {
                Created = DateTime.Now,
                OrderId = request.InvoiceDto.OrderId,
            };

            List<ProductOrderDto> productOrders = _uow.ProductOrderRepository.Query().Where(x => x.OrderId == invoice.OrderId).Select(ProductOrderDto.Projection).ToList();

            decimal fullPrice = 0;
            foreach (var item in productOrders)
            {
                ProductDto product = _uow.ProductRepository.Query().Where(x => x.Id == item.ProductId).Select(ProductDto.Projection).FirstOrDefault();
                _uow.ProductRepository.Query().Where(x => x.Id == item.ProductId).ToList().ForEach(x => x.Quantity = x.Quantity - item.Quantity);
                fullPrice += item.Quantity * (product.Price + product.Price * product.VAT / 100);
            }

            invoice.FullPrice = fullPrice;
            _uow.InvoiceRepository.Insert(invoice);

            _uow.SaveChanges();

            return VoidResult.Task;
        }
    }
}
