﻿using System;
using System.Collections.Generic;
using System.Text;
using Errand.Application.Order;

namespace Errand.Application.Invoice
{
    public class InvoiceDetailsDto
    {
        public int OrderId { get; set; }
        public int InvoiceId { get; set; }
        public OrderDetailsDto Order { get; set; }
    }
}
