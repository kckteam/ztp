﻿using System;
using System.Collections.Generic;
using System.Text;
using Errand.Domain.Entities.Base;

namespace Errand.Domain.Entities
{
    public class User : BaseEntity<int>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
