﻿using Errand.Domain.Entities.Base;

namespace Errand.Domain.Entities
{
    public class ProductOrder : BaseEntity<int>
    {
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}
