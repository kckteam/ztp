﻿using System;
using System.Collections.Generic;
using System.Text;
using Errand.Domain.Entities.Base;

namespace Errand.Domain.Entities
{
    public class Order: BaseEntity<int>
    {
        public string ClientName { get; set; }
        public string OrderState { get; set; }
        
        public virtual Invoice Invoice { get; set; }
        public ICollection<ProductOrder> ProductOrders { get; set; }
    }
}
