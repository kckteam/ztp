﻿using System.Collections.Generic;
using Errand.Domain.Entities.Base;

namespace Errand.Domain.Entities
{
    public class Product : BaseEntity<int>
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal VAT { get; set; }
        public int Quantity { get; set; }

        public ICollection<ProductOrder> ProductsOrders { get; set; }
    }
}
