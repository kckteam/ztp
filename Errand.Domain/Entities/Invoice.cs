﻿using System.Collections.Generic;
using Errand.Domain.Entities.Base;

namespace Errand.Domain.Entities
{
    public class Invoice : BaseEntity<int>
    {
        public decimal FullPrice { get; set; }
        public int OrderId { get; set; }

        public Order Order { get; set; }
    }
}
