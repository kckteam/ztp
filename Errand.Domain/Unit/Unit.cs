﻿using System;
using System.Threading.Tasks;

namespace Errand.Domain.Unit
{
    public struct VoidResult : IEquatable<VoidResult>, IComparable<VoidResult>, IComparable
    {
        public static readonly VoidResult Value = new VoidResult();

        public static readonly Task<VoidResult> Task = System.Threading.Tasks.Task.FromResult(Value);

        public int CompareTo(VoidResult other)
        {
            return 0;
        }

        public int CompareTo(object obj)
        {
            return 0;
        }

        public bool Equals(VoidResult other)
        {
            return true;
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public override bool Equals(object obj)
        {
            return obj is VoidResult;
        }

        public static bool operator ==(VoidResult first, VoidResult secound)
        {
            return true;
        }

        public static bool operator !=(VoidResult first, VoidResult secound)
        {
            return false;
        }

        public override string ToString()
        {
            return "()";
        }

    }
}
