﻿using System;
using System.Collections.Generic;
using System.Text;
using Errand.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Errand.Persistence.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasOne(x => x.Invoice).WithOne(i => i.Order)
                .HasForeignKey<Invoice>(x => x.OrderId);
        }
    }
}
