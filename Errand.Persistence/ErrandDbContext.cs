﻿using Errand.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Errand.Persistence
{
    public class ErrandDbContext : DbContext
    {
        public ErrandDbContext(DbContextOptions<ErrandDbContext> options) : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<ProductOrder> ProductOrders { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ErrandDbContext).Assembly);
        }
    }
}
