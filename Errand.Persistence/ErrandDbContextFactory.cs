﻿using Microsoft.EntityFrameworkCore;
using Errand.Persistence.Infrastructure;

namespace Errand.Persistence
{
    public class ErrandDbContextFactory : DesignTimeDbContextFactoryBase<ErrandDbContext>
    {
        protected override ErrandDbContext CreateNewInstance(DbContextOptions<ErrandDbContext> options)
        {
            return new ErrandDbContext(options);
        }
    }
}
