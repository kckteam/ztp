﻿namespace Errand.Infrastucture
{
    using System;
    using System.Linq;
    using Errand.Application.Interfaces;
    using Errand.Domain.Entities.Base;
    using Errand.Persistence;

    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>, new()
        where TKey : IComparable
    {
        private readonly ErrandDbContext _context;
        private bool _disposed;

        public Repository(ErrandDbContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void Delete(TEntity entity)
        {
            _context.Remove(entity);
        }

        public TEntity Insert(TEntity entity)
        {
            _context.Add(entity);
            return entity;
        }

        public IQueryable<TEntity> Query()
        {
            return _context.Set<TEntity>().AsQueryable();
        }

        public TEntity Update(TEntity entity)
        {
            _context.Update(entity);
            return entity;
        }

        public void Dispose()
        {
            _disposed = true;
        }
    }
}
