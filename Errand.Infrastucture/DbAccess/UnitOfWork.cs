﻿namespace Errand.Infrastucture
{
    using System;
    using System.Collections;
    using System.Threading;
    using System.Threading.Tasks;
    using Errand.Application.Interfaces;
    using Errand.Domain.Entities;
    using Errand.Domain.Entities.Base;
    using Errand.Persistence;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly ErrandDbContext _context;

        private bool _disposed;

        private Hashtable _repositories;

        public UnitOfWork(ErrandDbContext context)
        {
            _context = context;
        }

        public IRepository<User, int> UserRepository { get => Repository<User, int>(); }

        public IRepository<ProductOrder, int> ProductOrderRepository { get => Repository<ProductOrder, int>(); }

        public IRepository<Product, int> ProductRepository { get => Repository<Product, int>(); }

        public IRepository<Invoice, int> InvoiceRepository { get => Repository<Invoice, int>(); }

        public IRepository<Order, int> OrderRepository { get => Repository<Order, int>(); }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _context.Dispose();
                if (_repositories != null)
                {
                    foreach (IDisposable repository in _repositories.Values)
                    {
                        repository.Dispose();
                    }
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IRepository<TEntity, TKey> Repository<TEntity, TKey>()
            where TEntity : BaseEntity<TKey>
            where TKey : IComparable
        {
            if (_repositories == null)
            {
                _repositories = new Hashtable();
            }

            var type = typeof(TEntity).Name;
            if (_repositories.ContainsKey(type))
            {
                return (IRepository<TEntity, TKey>)_repositories[type];
            }

            var repositoryType = typeof(Repository<,>);
            _repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(new Type[] { typeof(TEntity), typeof(TKey) }), _context));
            return (IRepository<TEntity, TKey>)_repositories[type];
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
    }
}
