﻿using System.Threading.Tasks;
using Errand.Application.Interfaces;
using Errand.Application.Notifications.Models;

namespace Errand.Infrastucture
{
    public class NotificationService : INotificationService
    {
        public Task SendAsync(Message message)
        {
            return Task.CompletedTask;
        }
    }
}
