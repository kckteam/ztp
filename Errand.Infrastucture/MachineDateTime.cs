﻿using System;
using Errand.Common;

namespace Errand.Infrastucture
{
    public class MachineDateTime : IDateTime
    {
        public DateTime Now => DateTime.Now;

        public int CurrentYear => DateTime.Now.Year;
    }
}
