﻿namespace Errand.Infrastucture.Logger
{
    using System;
    public class ConsoleLogger : LogStrategy
    {
        protected override bool DoLog(string logitem)
        {
            Console.WriteLine(logitem + "\r\n");
            return true;
        }
    }
}
