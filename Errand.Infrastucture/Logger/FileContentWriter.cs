﻿namespace Errand.Infrastucture.Logger
{
    using System.IO;
    using System.Text;
    using Errand.Application.Interfaces.Logger;
    public class FileContentWriter : BaseContentWriter
    {
        private string _fileName;
        public FileContentWriter(string fileName)
        {
            _fileName = fileName;
        }
        public override bool WriteToMedia(string content)
        {
            using (FileStream SourceStream =
            File.Open(_fileName, FileMode.Append))
            {
                byte[] buffer =
                Encoding.UTF8.GetBytes(content + "\r\n");
                SourceStream.Write(buffer, 0, buffer.Length);
            }
            return true;
        }
    }

}
