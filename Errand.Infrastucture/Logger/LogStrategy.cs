﻿using System;
using Errand.Application.Interfaces.Logger;

namespace Errand.Infrastucture.Logger
{
    public abstract class LogStrategy : ILogger
    {
        protected abstract bool DoLog(string logitem);
        public bool Log(string app, string key, string cause)
        {
            return DoLog(app+"-" + DateTime.Now.ToShortDateString() + "T" + DateTime.Now.ToShortTimeString() + " [" + key + "] " + cause);
        }

        public static LogStrategy CreateLogger(string loggertype)
        {
            if (loggertype == "FILE")
                return new FileLogger();
            else
                return new ConsoleLogger();
        }
    }
}
