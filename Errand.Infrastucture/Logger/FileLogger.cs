﻿namespace Errand.Infrastucture.Logger
{
    using System;
    using Errand.Application.Interfaces.Logger;

    public class FileLogger : LogStrategy
    {
        BaseContentWriter wt = new FileContentWriter("log" + DateTime.Now.ToFileTimeUtc()+ ".txt");
        protected override bool DoLog(String logitem)
        {
            // Log into the file 
            wt.Write(logitem);
            return true;
        }
    }
}